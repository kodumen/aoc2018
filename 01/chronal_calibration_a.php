<?php

function main(string $input) {
    return array_sum(explode("\n", $input));
}

echo main(file_get_contents(__DIR__ . '/input.txt')) . "\n";