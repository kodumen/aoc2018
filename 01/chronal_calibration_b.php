<?php

function main(string $input) {
    $frequency = 0;
    $results = [0 => true];

    while (true) {
        foreach (explode("\n", $input) as $delta) {
            $frequency += (int)($delta);

            if ($results[$frequency] ?? false) {
                return $frequency;
            }

            $results[$frequency] = true;
        }
    }
}

echo main(file_get_contents(__DIR__ . '/input.txt')) . "\n";