#! /usr/bin/python3

def parse_id(box_id):
    has_thrice = False
    has_twice = False

    box_id = box_id.strip()
    for char in box_id:
        if (box_id.count(char) == 3):
            has_thrice = True
        elif (box_id.count(char) == 2):
            has_twice = True
        elif has_thrice and has_twice:
            break

    
    return int(has_twice), int(has_thrice)
            
    
twice_count = 0
thrice_count = 0    

with open('input.txt') as box_ids:
    for box_id in box_ids:
        twice, thrice = parse_id(box_id)
        twice_count += twice
        thrice_count += thrice
    
print(twice_count * thrice_count)