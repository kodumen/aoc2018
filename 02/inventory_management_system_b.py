#! /usr/bin/python3

box_ids = []

with open('input.txt') as input:
    box_ids = input.readlines()

def get_box_id_diff(box_id1, box_id2):
    diff_count = 0
    for i in range(len(box_id1.strip())):
        diff_count += int(box_id1[i] != box_id2[i])
    return diff_count

def get_matching_characters(box_id1, box_id2):
    matches = ''
    for i in range(len(box_id1.strip())):
        if (box_id1[i] == box_id2[i]):
            matches += box_id1[i]
    return matches

for box_id in box_ids:
    for target in box_ids:
        if get_box_id_diff(box_id, target) == 1:
            print(get_matching_characters(box_id, target))
            exit()
